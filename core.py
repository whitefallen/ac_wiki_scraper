import codecs
import requests
import json
from bs4 import BeautifulSoup


def resolve_month(monthtds):
    months = []
    for month in monthtds:
        if month.get_text().lstrip(' ').rstrip('\n') != '-':
            months.append('Yes')
        else:
            months.append('No')
    return months


def build_json_data_fish(datatablerows, URL):
    json_data = {'title': 'Animal Crossing Wiki Scraping (Fish)', 'url': URL, 'data': []}
    i = 1
    print(json_data)
    for tr in datatablerows:
        alltds = tr.find_all('td')
        monthtds = alltds[6:]
        resolved_months = resolve_month(monthtds)
        json_data['data'].append({'id': i,
                                  'name': alltds[0].get_text().lstrip(' ').rstrip('\n'),
                                  'image': alltds[1].get_text().lstrip(' ').rstrip('\n'),
                                  'price': alltds[2].get_text().lstrip(' ').rstrip('\n'),
                                  'location': alltds[3].get_text().lstrip(' ').rstrip('\n'),
                                  'shadow size': alltds[4].get_text().lstrip(' ').rstrip('\n'),
                                  'time': alltds[5].get_text().lstrip(' ').rstrip('\n'),
                                  'month': resolved_months
                                  })
        i = i + 1
    return json_data


def generate_json_file(_json_data, _filename):
    with open(_filename, 'w') as outfile:
        json.dump(_json_data, outfile, indent=4)


URL = 'https://animalcrossing.fandom.com/wiki/Fish_(New_Horizons)'

page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')
soup.encode("utf-8")

results = soup.select('table.roundy.sortable')
northernHemisphere = results[0]
southernHemisphere = results[1]

searchedTableRows = northernHemisphere.find_all('tr')
searchedTableRows_south = southernHemisphere.find_all('tr')
newTableRows = searchedTableRows[1:]
newTableRows_south = searchedTableRows_south[1:]
print(newTableRows_south[0].prettify())

extracted_json_data_north = build_json_data_fish(newTableRows, URL)
extracted_json_data_south = build_json_data_fish(newTableRows_south, URL)
generate_json_file(extracted_json_data_north, 'north_data.json')
generate_json_file(extracted_json_data_south, 'south_data.json')

filename = 'row_example_body.txt'
wrapped = searchedTableRows[2].prettify()
f = codecs.open(filename, 'w', 'utf-8')
f.write(wrapped)
f.close()

filename = 'page_example_body.txt'
wrapped = northernHemisphere.prettify()
f = codecs.open(filename, 'w', 'utf-8')
f.write(wrapped)
f.close()
