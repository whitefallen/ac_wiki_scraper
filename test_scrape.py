import codecs
import requests
import json
from bs4 import BeautifulSoup

URL = 'https://animalcrossing.fandom.com/wiki/Fish_(New_Horizons)#Northern%20Hemisphere'
page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')
soup.encode("utf-8")

results = soup.select('table.roundy.sortable')

json_data = {}

json_data['title'] = 'Animal Crossing Wiki Scraping (Fish)'
json_data['url'] = URL

trHeadingChilds = results[0].tr.findChildren()
for th in trHeadingChilds:
    cleanedTh = th.get_text().rstrip('\n').lstrip(' ')
    print(cleanedTh)
    json_data[cleanedTh] = cleanedTh

with open('result_set.json', 'w') as outfile:
    json.dump(json_data, outfile, indent=4)

filename = 'page_body.txt'
wrapped = results[0].prettify()
f = codecs.open(filename, 'w', 'utf-8')
f.write(wrapped)
f.close()
